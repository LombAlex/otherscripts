# Create the folders
mkdir ./Converted

for folder in */ ; do
        mkdir "./Converted/${folder}" 
done

for folder in */*/ ; do
        mkdir "./Converted/${folder}" 
done

# Remove the folder Converted 
rm -dr ./Converted/Converted

# Convert the flac, wav files
for file in *.flac
do
        ffmpeg -i "$file" ./Converted/"${file%.*}".mp3
done

for file in *.wav
do
        ffmpeg -i "$file" ./Converted/"${file%.*}".mp3
done

# Convert the flac, wav files inside a directory
for file in */*.flac
do
        ffmpeg -i "$file" ./Converted/"${file%.*}".mp3
done

for file in */*.wav
do
        ffmpeg -i "$file" ./Converted/"${file%.*}".mp3
done

# Convert the flac, wav files inside a directory in a directory
for file in */*/*.flac
do
        ffmpeg -i "$file" ./Converted/"${file%.*}".mp3
done

for file in */*/*.wav
do
        ffmpeg -i "$file" ./Converted/"${file%.*}".mp3
done

# Copy the mp3 files
for file in *.mp3
do
        cp "${file}" ./Converted/"${file}"
done

for file in */*.mp3
do
        cp "${file}" ./Converted/"${file}"
done

for file in */*/*.mp3
do
        cp "${file}" ./Converted/"${file}"
done

echo "Done"