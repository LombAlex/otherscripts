from reportlab.pdfgen import canvas
from reportlab.lib.units import cm
import os, os.path

'''
Créer un fichier pdf a partir d'images
#ATTENTION nécessite
	Même nom des fichiers: 1.jpg 2 3 4 5 6 7 8 9...
	Même nom de chapitre
'''


NOM_CHAPITRE = "Album ch."
NOM_IMAGE = ""  #si vide alors "1.jpg", "2.jpg" ...
PREMIERE_IMAGE = 0 #si la première image du chapitre est 0 ou 1
FORMAT = ".jpg"  #.jpg, .png, .jp2 fonctionne
CHAPITRE_DEBUT = 1
CHAPITRE_FIN = 20  #len([name for name in os.listdir('.') if os.path.isfile(name)]) #Nb de fichier <= nb de chapitre
BONUS = False  #Les bonus c'est les par exemple 1.5 (seulement en .5)
NOM_PDF = "Album.pdf"

c = canvas.Canvas(NOM_PDF+".pdf")
for j in range(CHAPITRE_DEBUT,CHAPITRE_FIN+1):
	for i in range(PREMIERE_IMAGE,len(os.listdir("./"+NOM_CHAPITRE+str(j)))+PREMIERE_IMAGE):
		c.drawImage("./"+NOM_CHAPITRE+str(j)+"/"+NOM_IMAGE+str(i)+FORMAT, 0, 0, width=21*cm, height=29.7*cm, preserveAspectRatio=True)
		c.showPage()
	if BONUS:
		try:
			for i in range(PREMIERE_IMAGE,len(os.listdir("./"+NOM_CHAPITRE+str(j)+".5"))+PREMIERE_IMAGE):
				c.drawImage("./"+NOM_CHAPITRE+str(j)+".5/"+NOM_IMAGE+str(i)+FORMAT, 0, 0, width=21*cm, height=29.7*cm, preserveAspectRatio=True)
				c.showPage()
		except:
			pass
c.save()
