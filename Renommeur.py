from os import listdir, rename, chdir
from os.path import isfile

'''
Renomme tout les dossiers dans un dossier
Ne renomme pas les fichiers !
Dossier1, Dossier2 ...
Ordre alphabétique !
'''

DOSSIER="./DossierParent"
NOM="Dossier"

fichiers = listdir(DOSSIER)
chdir(DOSSIER)
compteur=1
for fichier in fichiers:
	if(isfile(fichier)==False):
		rename(fichier, NOM+str(compteur))
		compteur+=1